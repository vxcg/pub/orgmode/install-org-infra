# This makefile drives the installation
# of org infrastructure.

SHELL:=/bin/bash
home=$(shell echo $$HOME)
# Custom parameters
# =================
# destination of the org infra
# CUSTOMIZABLE
dest=~/tmp/org-infra
force=
# force=true

utils-dir=${dest}/utils
publishers-dir=${dest}/publishers
exporters-dir=${dest}/exporters
org-distros-dir=${dest}/org-distros
themes-dir=${dest}/themes
models-dir=${dest}/models

# command to run emacs, emacs >=27.2 needed
# CUSTOMIZABLE
emacs=emacs

# Emacs Utilities; others may be similarly specified
# -----------------
emacs-utils-repo=https://gitlab.com/vxcg/pub/emacs/emacs-utils.git
emacs-utils-dir=${utils-dir}/emacs-utils

# CUSTOMIZABLE
# ------------
publisher-repo=https://gitlab.com/vxcg/pub/orgmode/el-publisher.git
publisher-dir=${publishers-dir}/el-publisher

# CUSTOMIZABLE
# ----------------------------
exporter-repo=https://gitlab.com/vxcg/pub/orgmode/html-exports/org-v9.git
exporter-dir=${exporters-dir}/org-v9

# CUSTOMIZABLE
# ----------------------------
org-distro-repo=https://git.savannah.gnu.org/git/emacs/org-mode.git
org-version=9.5
org-distro-dir=${org-distros-dir}/org-mode-${org-version}

# CUSTOMIZABLE
# ----------------------------
model-repo=https://gitlab.com/vxcg/pub/orgmode/html-exports/model-v9.git
model-dir=${models-dir}/model-v9


# -----------------------------------------------
# assumes npm (node package manager) is installed
decktape=decktape

usage:
	echo "make -k [dest=<dest-dir:default:/tmp/org-infra>] all"

all:  init utils exporter themes model

init:
	(mkdir -p ${utils-dir} ${publishers-dir} \
	${exporters-dir} ${org-distros-dir} ${themes-dir} ${models-dir})

utils: init
	(source ./init.sh; git-install ${emacs-utils-dir} ${emacs-utils-repo})

publisher: utils
	(source ./init.sh; git-install ${publisher-dir} ${publisher-repo})

exporter: init
	(source ./init.sh; git-install ${exporter-dir} ${exporter-repo})

exporter-init: exporter
	(cd ${exporter-dir} && make -k dest=${dest} all)

org-distro:
	(source ./init.sh; cd ${org-distros-dir}; \
	git-install ${org-distro-dir} ${org-distro-repo} \
    "--depth 1 --branch release_${org-version}" )

model:
	(source ./init.sh; cd ${models-dir}; \
	git-install ${model-dir} ${model-repo})

include ./themes.mk


