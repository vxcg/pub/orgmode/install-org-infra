# THEMES
# ----------------------------------------------
algo-repo=https://gitlab.com/vxcg/pub/orgmode/html-themes/algo.git
readtheorg-repo=https://gitlab.com/vxcg/pub/orgmode/html-themes/readtheorg.git
# readthorg-repo=https://fniessen.github.io/org-html-themes/org/theme-readtheorg.setup
book-repo=https://gitlab.com/vxcg/pub/orgmode/html-themes/book.git
ibp-repo=https://gitlab.com/vxcg/res/t4e/ibp.git
popl-repo=https://gitlab.com/vxcg/pub/orgmode/html-themes/popl.git
math-repo=https://gitlab.com/vxcg/pub/orgmode/html-themes/math.git
vlead-slides-repo=https://gitlab.com/vxcg/pub/orgmode/html-themes/vlead-slides.git
iiit-slides-repo=https://gitlab.com/vxcg/pub/orgmode/html-themes/iiit-slides.git
experiment-repo=https://gitlab.com/vxcg/pub/orgmode/html-themes/experiment.git
org-reveal-iiit-repo=https://gitlab.com/vxcg/pub/orgmode/html-themes/org-reveal-iiit.git
reveal-js-repo=https://github.com/hakimel/reveal.js.git
reveal-js-toc-progress-repo=https://github.com/e-gor/Reveal.js-TOC-Progress.git
themes: init
	(source ./init.sh; cd ${themes-dir}; \
	git-install-repo ${algo-repo}; \
	git-install-repo ${readtheorg-repo}; \
	git-install-repo ${book-repo}; \
	git-install-repo ${ibp-repo}; \
	git-install-repo ${popl-repo}; \
	git-install-repo ${math-repo}; \
	git-install-repo ${vlead-slides-repo}; \
	git-install-repo ${iiit-slides-repo}; \
	git-install-repo ${org-reveal-iiit-repo}; \
	git-install-repo ${reveal-js-repo})

#	git-install-repo ${experiment-repo}; \
