# This makefile is incomplete.
# When complete it will  install org-reveal infra

plugin:
	(mkdir -p ${src-dir}/plugin; cd ${src-dir}/plugin; \
    ln -sf ../themes/org-reveal/Reveal.js-TOC-Progress/plugin/toc-progress)

slides: plugin themes
	(${emacs} -q --script ${exp-dir}/elisp/publish-cli-proxy.el \
	"${emacs-utils-dir}:${exp-dir}/elisp" ${prj-dir} org-reveal)

# assumes decktape is installed
reveal-pdf:
	@echo "conv to pdf using decktape"
	${decktape} -s 1280x760 reveal src/index.html src/index.pdf

# uncomment this if you wish to add more make commands in extra.mk
# include extra.mk

